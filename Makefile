.PHONY = all compile run help

all: compile run ##

loadnvm: ##
	. ${HOME}/.nvm/nvm.sh && nvm use

build: loadnvm ##
	npm run build

run: loadnvm ##
	npm start

test: loadnvm ##
	npm run test:e2e

help:
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/$$//' | sed -e 's/##//'
	@echo ""
