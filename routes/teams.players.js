import createRouter from './default.router.js'

const data = [
  { id: '0', name: 'John Doe', position: 'Forward' },
  { id: '1', name: 'Jane Doe', position: 'Midfielder' },
  { id: '2', name: 'Alice Smith', position: 'Defender' },
  { id: '3', name: 'Bob Johnson', position: 'Goalkeeper' },
  { id: '4', name: 'Michael Brown', position: 'Forward' },
  { id: '5', name: 'Emily Davis', position: 'Midfielder' },
  { id: '6', name: 'Oliver Wilson', position: 'Defender' },
  { id: '7', name: 'Sophia Taylor', position: 'Goalkeeper' },
  { id: '8', name: 'Matthew Anderson', position: 'Forward' },
  { id: '9', name: 'Ava Martinez', position: 'Midfielder' },
  { id: '10', name: 'William Thomas', position: 'Defender' },
  { id: '11', name: 'Isabella Garcia', position: 'Goalkeeper' },
  { id: '12', name: 'James Robinson', position: 'Forward' },
  { id: '13', name: 'Mia Clark', position: 'Midfielder' },
  { id: '14', name: 'Benjamin Rodriguez', position: 'Defender' },
  { id: '15', name: 'Charlotte Lewis', position: 'Goalkeeper' },
  { id: '16', name: 'Daniel Lee', position: 'Forward' },
  { id: '17', name: 'Amelia Walker', position: 'Midfielder' },
  { id: '18', name: 'Joseph Hall', position: 'Defender' },
  { id: '19', name: 'Grace Young', position: 'Goalkeeper' },
]

const router = createRouter({mergeParams: true}, data, 'player')

export default router
