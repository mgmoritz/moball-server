import express from 'express';
const app = express()
import cors from 'cors'
import logger from 'morgan';

import IndexRouter from './routes/index.js'
import TeamsRouter from './routes/teams.js'


app.use(logger('dev'));
app.use(cors());
app.use(express.json());

app.use('/', IndexRouter);
app.use('/teams', TeamsRouter);

export default app
