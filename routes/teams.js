import { Router } from 'express';
import PlayersRouter from './teams.players.js'
import EventsRouter from './teams.events.js'
import createRouter from './default.router.js'

const data = [
  { id: '0', name: 'Q-Fase', emblemUrl: '', fairPlayPoints: 0, commitmentPoints: 0 },
  { id: '1', name: 'Amigos do Futebol', emblemUrl: '', fairPlayPoints: 0, commitmentPoints: 0 },
]

const router = createRouter({}, data, 'team')

router.use('/:teamId/players', PlayersRouter);
router.use('/:teamId/events', EventsRouter);

export default router
