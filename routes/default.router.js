import { Router } from 'express';
function createRouter(options, data, itemName='item') {
  const router = Router(options);

  router.get('/', function(req, res, next) {
    res.json(data)
  });

  router.post('/', function(req, res, next) {
    const item = data.find(item => item.id === itemId)
    if (!item) {
      data.push(req.body)
      res.send(201)
    } else {
      res.send(404, 'Item already exists')
    }
  })

  router.put('/', function(req, res, next) {
    const itemId = req.body.id
    if (!itemId) return res.send(400)

    const item = data.find(item => item.id === itemId)
    if (!item) {
      data.push(req.body)
      res.send(201)
    } else {
      Object.assign(item, req.body)
      res.send(204)
    }
  })

  router.get(`/:${itemName}Id`, function(req, res, next) {
    var item = data.find(item => item.id === req.params[`${itemName}Id`])
    if (!item)
      return res.send(404)

    res.json(item)
  })

  router.delete(`/:${itemName}Id`, function(req, res, next) {
    var item = data.find(item => item.id === req.params[`${itemName}Id`])
    data.splice(data.indexOf(item), 1)
    res.send(204)
  })

  return router
}

export default createRouter
