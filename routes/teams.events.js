import createRouter from './default.router.js'

const data = [
  { id: '0', name: 'Jogo 1', date: '2024-03-01 20:30' },
  { id: '1', name: 'Jogo 2', date: '2024-03-08 20:30' },
]

const router = createRouter({mergeParams: true}, data, 'event')

export default router
